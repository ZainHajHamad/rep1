package RestAssuredTesting;

import org.testng.Assert;
import org.testng.annotations.Test;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class TC6_DELETE_Request {
	@Test
	public void deleteAllEmployees() throws Exception{
		
	
		 RestAssured.baseURI = "http://192.168.200.91:8080/demo-server/employee-module";
		 RequestSpecification request = RestAssured.given(); 
		 
		 // Add a header stating the Request body is a JSON
		 request.header("Content-Type", "application/json"); 
		 
		       // Delete the request and check the response
		 Response response = request.delete("/zain"); 
		
		  int statusCode=response.getStatusCode();
		  System.out.println("Status code is: "+statusCode);
		  Assert.assertEquals(statusCode, 200);
		  
		 
		  String responseBody=response.getBody().asString();
		  System.out.println("Response Body is:" +responseBody);
		 
		  //status line verification
		  String statusLine=response.getStatusLine();
		  System.out.println("Status line is:"+statusLine);
		  Assert.assertEquals(statusLine, "HTTP/1.1 200 CatoOK");
	}
		
}
