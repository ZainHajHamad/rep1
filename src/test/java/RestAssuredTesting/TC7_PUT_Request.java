package RestAssuredTesting;

import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class TC7_PUT_Request {
	@Test
	public void UpdateEmployeeInfoById() throws Exception{
		{
		        RestAssured.baseURI = "http://192.168.200.91:8080/demo-server/employee-module/4352324648";
		     
		        RequestSpecification httpRequest = RestAssured.given();
		 
		        //Passing the data to be updated
		        JSONObject updateData = new JSONObject();
		        updateData.put("firstName", "Zain");
		 
		        httpRequest.header("Content-Type", "application/json");
		 
		        httpRequest.body(updateData.toJSONString());
		        Response response = httpRequest.request(Method.PUT, "4");
		 

				 //status code validation
				  int statusCode=response.getStatusCode();
				  System.out.println("Status code is: "+statusCode);
				  Assert.assertEquals(statusCode, 200); 
		 
		        //Checking in response if data is updated
		        JsonPath newData = response.jsonPath();
		        String name = newData.get("firstName");
		        System.out.println("firstName is: "+name);
		    
			 
		}
	}
}
