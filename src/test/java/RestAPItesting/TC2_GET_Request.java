package RestAPItesting;

import static org.testng.Assert.assertEquals;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import org.apache.groovy.json.internal.LazyMap;
import org.testng.annotations.Test;
import groovy.json.JsonSlurper;

public class TC2_GET_Request {

	@Test
	//test the response code
	//take a small range inside the company's salaries range and having at least one employee in this range. 
	public void testResponceCodeForValidSalariesInsideCompanyRange() throws Exception{
		URL url = new URL("http://192.168.200.91:8080/demo-server/employee-module/zain/9000/10000");
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("GET");
        //status code validation
        int statusCode = con.getResponseCode();
        assertEquals(200,statusCode);
	}
	
	@Test
	//test the response body
	//take a small range inside the company's salaries range and there is at least one employee has a salary in this range. 
	public void testResponceBodyForValidSalariesInsideCompanyRange() throws Exception {
		URL url = new URL("http://192.168.200.91:8080/demo-server/employee-module/zain/9000/10000");
	    HttpURLConnection con = (HttpURLConnection) url.openConnection();
	    BufferedReader buffer = new BufferedReader(new InputStreamReader((con.getInputStream())));
        StringBuilder st= new StringBuilder();
	    String output;
	    System.out.println("\nHere is the output of test Responce Body For Valid Salaries Inside Company Range:");
	    while ((output = buffer.readLine()) != null) {
	        st.append(output); 
	    }
	    buffer.close();
	    System.out.println(st.toString());
	   
	  int f=1;
	  JsonSlurper json = new JsonSlurper();
      LazyMap map = (LazyMap) json.parseText(st.toString());  
      ArrayList items = (ArrayList)  map.get("items"); 
      for (int i=0; i<items.size();i++) {
    	  for ( Object object_item : items ) {
    		  LazyMap lazy_map_item = (LazyMap)object_item;
    		  if (((Integer) lazy_map_item.get("salary") <=9000 ) || ((Integer)lazy_map_item.get("salary") >=10000 )) f =0;
    	  }
      }
      assertEquals(f,1); //checking that there is an employee has a salary in the specified range(9000-10000).
      LazyMap item =(LazyMap)items.get(0);
      assertEquals(item.get("firstName"),"Blaise");
      assertEquals(item.get("lastName"),"Pascal");
      assertEquals(item.get("id"),"438745745094");
      assertEquals(item.get("salary"),10000); 
	  con.disconnect();
	  }	
	
}
