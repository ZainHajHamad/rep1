package RestAPItesting;

import static org.testng.Assert.assertEquals;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import org.apache.groovy.json.internal.LazyMap;
import org.testng.annotations.Test;

import groovy.json.JsonSlurper;

public class TC4_GET_Request {
	@Test
	//test the response code
	//take a range out of the company's salaries range. 
	public void testResponceCodeForSalariesOutOfCompanyRangeSalaries()  throws Exception {
		URL url = new URL("http://192.168.200.91:8080/demo-server/employee-module/zain/15000/20000");
	    HttpURLConnection con = (HttpURLConnection) url.openConnection();
	    con.setRequestMethod("GET");
	    //checking the response code
	    int statusCode = con.getResponseCode() ;
	    assertEquals(400,statusCode); //checking that there are no employee has a salary out of company's range salaries
	    con.disconnect();
	  }	
	@Test
	//test the response body
	//take a range out of the company's salaries range.
	public void  testResponceBodyForSalariesOutOfCompanyRangeSalaries() throws Exception {
		URL url = new URL("http://192.168.200.91:8080/demo-server/employee-module/zain/15000/20000");
	    HttpURLConnection con = (HttpURLConnection) url.openConnection();
	    BufferedReader buffer = new BufferedReader(new InputStreamReader((con.getInputStream())));
	    StringBuilder st= new StringBuilder();
	    String output;
	    System.out.println("Here is the output of test Responce Body For Salaries Out Of Company Range Salaries:");
	    while ((output = buffer.readLine()) != null) {
	        st.append(output); 
	    }
	    buffer.close();
	    System.out.println(st.toString());
	   JsonSlurper json = new JsonSlurper();
	  LazyMap my_map = (LazyMap) json.parseText(st.toString());  
	  ArrayList items = (ArrayList)my_map.get("items");    
	  int arrayListSize = items.size(); 
	  assertEquals(arrayListSize,0); 
	  con.disconnect();
	  }	


}
