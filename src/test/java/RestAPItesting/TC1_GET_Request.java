package RestAPItesting;

import static org.testng.Assert.assertEquals;
import groovy.json.JsonSlurper;
import java.io.BufferedReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import org.apache.groovy.json.internal.LazyMap;
import org.testng.annotations.Test;
import java.io.InputStreamReader;


public class TC1_GET_Request {
	@Test
	public void testResponceCodeForGETAllEmployees() throws Exception{
		URL url = new URL("http://192.168.200.91:8080/demo-server/employee-module/zain");
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("GET");
        //checking validation of Response code
        int statusCode = con.getResponseCode() ;
        assertEquals(200,statusCode);
	}
	
	@Test 
	public void testResponseBodyForGETAllEmployees() throws Exception {
		URL url = new URL("http://192.168.200.91:8080/demo-server/employee-module/zain");
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        BufferedReader buffer = new BufferedReader(new InputStreamReader((con.getInputStream())));

        StringBuilder st= new StringBuilder();
	    String output;
	    System.out.println("\nHere is the Body Response for Getting all employees:");
	    while ((output = buffer.readLine()) != null) {
	    st.append(output); 
	    }
	    buffer.close();
	    System.out.println(st.toString());
        JsonSlurper json = new JsonSlurper();
        LazyMap map = (LazyMap) json.parseText(st.toString());  
        ArrayList array_list = (ArrayList)map.get("items");    
        int arrayListSize = array_list.size(); 
        assertEquals(arrayListSize,2); //check that there are two employees in the list.
        con.disconnect();
      }	
}
